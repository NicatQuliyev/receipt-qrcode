package az.nicat.receiptqrcode.repository;

import az.nicat.receiptqrcode.entity.GroceryItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroceryItemRepository extends JpaRepository<GroceryItem, Long> {
}
