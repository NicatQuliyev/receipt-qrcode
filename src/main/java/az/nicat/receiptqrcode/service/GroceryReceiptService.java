package az.nicat.receiptqrcode.service;

import az.nicat.receiptqrcode.entity.GroceryItem;
import az.nicat.receiptqrcode.repository.GroceryItemRepository;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import lombok.RequiredArgsConstructor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class GroceryReceiptService {

    private final GroceryItemRepository itemRepository;

    public List<GroceryItem> getAllItems() {
        return itemRepository.findAll();
    }

    public File generateReceipt(List<GroceryItem> items) throws IOException, WriterException {
        File tempFile = File.createTempFile("grocery_receipt", ".pdf");
        PdfWriter finalWriter = new PdfWriter(new FileOutputStream(tempFile));
        PdfDocument finalPdf = new PdfDocument(finalWriter);
        Document finalDocument = new Document(finalPdf);

        finalDocument.add(new Paragraph("Market Name"));
        finalDocument.add(new Paragraph("Address: 123 Market Street"));
        finalDocument.add(new Paragraph("City, State, ZIP: Marketville, MV, 12345"));
        finalDocument.add(new Paragraph("Phone: (123) 456-7890"));

        finalDocument.add(new Paragraph("------------------------"));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = dateFormat.format(new Date());
        finalDocument.add(new Paragraph("Date: " + formattedDate));

        finalDocument.add(new Paragraph("Market Receipt"));

        for (GroceryItem item : items) {
            finalDocument.add(new Paragraph(
                    String.format("%-10s %-20s $%-10.2f $%-10.2f", item.getQuantity(), item.getName(),
                            item.getPrice(), item.getQuantity() * item.getPrice())
            ));
        }

        double subtotal = items.stream()
                .mapToDouble(item -> item.getPrice() * item.getQuantity())
                .sum();

        finalDocument.add(new Paragraph(String.format("Subtotal:\t\t\t\t\t\t\t\t\t\t\t\t$%.2f", subtotal)));

        double tax = subtotal * 0.08;
        finalDocument.add(new Paragraph(String.format("Tax (8%%):\t\t\t\t\t\t\t\t\t\t\t\t\t$%.2f", tax)));

        double total = subtotal + tax;
        finalDocument.add(new Paragraph(String.format("Total:\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t$%.2f", total)));

        String jsonData = itemsToJson(items);
        int size = 150;
        BitMatrix bitMatrix = new QRCodeWriter().encode(jsonData, BarcodeFormat.QR_CODE, size, size);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        MatrixToImageWriter.writeToStream(bitMatrix, "PNG", baos);
        Image qrImage = new Image(ImageDataFactory.create(baos.toByteArray()));

        finalDocument.add(qrImage);

        finalDocument.close();

        return tempFile;
    }

    private String itemsToJson(List<GroceryItem> items) {
        JSONArray jsonArray = new JSONArray();
        for (GroceryItem item : items) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", item.getName());
            jsonObject.put("price", item.getPrice());
            jsonObject.put("quantity", item.getQuantity());
            jsonArray.add(jsonObject);
        }
        return jsonArray.toJSONString();
    }

    public void addItem(List<GroceryItem> items) {
        itemRepository.saveAll(items);
    }
}
