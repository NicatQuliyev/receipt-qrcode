package az.nicat.receiptqrcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class ReceiptQrCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReceiptQrCodeApplication.class, args);
    }

}
