package az.nicat.receiptqrcode.controller;

import az.nicat.receiptqrcode.entity.GroceryItem;
import az.nicat.receiptqrcode.service.GroceryReceiptService;
import com.google.zxing.WriterException;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

@RestController
@AllArgsConstructor
public class GroceryReceiptController {

    private GroceryReceiptService receiptService;

    @GetMapping("/generateReceipt")
    public void generateReceipt(HttpServletResponse response) throws IOException, WriterException {
        List<GroceryItem> items = receiptService.getAllItems();

        File tempFile = receiptService.generateReceipt(items);

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=grocery_receipt_with_qr.pdf");

        Files.copy(tempFile.toPath(), response.getOutputStream());
        response.flushBuffer();

        tempFile.delete();
    }

    @PostMapping("/addItem")
    public void addItem(@RequestBody List<GroceryItem> items, HttpServletResponse response) throws IOException, WriterException {
        receiptService.addItem(items);

        List<GroceryItem> allItems = receiptService.getAllItems();
        File tempFile = receiptService.generateReceipt(allItems);

        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=grocery_receipt_with_qr.pdf");

        Files.copy(tempFile.toPath(), response.getOutputStream());
        response.flushBuffer();

        tempFile.delete();
    }

}
